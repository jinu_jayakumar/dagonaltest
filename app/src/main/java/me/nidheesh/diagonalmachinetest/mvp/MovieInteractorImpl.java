package me.nidheesh.diagonalmachinetest.mvp;

import android.content.Context;
import android.content.res.Resources;

import me.nidheesh.diagonalmachinetest.R;
import me.nidheesh.diagonalmachinetest.model.MovieList;

import static me.nidheesh.diagonalmachinetest.provider.ApiProvider.getMovieList;

/**
 * Created by nidheesh on 27/09/2016.
 **/
class MovieInteractorImpl implements MovieInteractor {


    @Override
    public void getMovies(Context context, int pageNum, MoviePresenter.GetList getList) {
        try {
            MovieList movieList = getMovieList(context.getResources().openRawResource(getContent(pageNum)));
            getList.onLoadList(movieList.page);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
            getList.noMoreItems();
        }


    }

    private  int getContent(int pageNum) {
        if (pageNum == 0) {
            return R.raw.contentlistingpage_page1;
        }
        if (pageNum == 1) {
            return R.raw.contentlistingpage_page2;
        }
        if (pageNum == 2) {
            return R.raw.contentlistingpage_page3;
        }
        return 0;
    }
}