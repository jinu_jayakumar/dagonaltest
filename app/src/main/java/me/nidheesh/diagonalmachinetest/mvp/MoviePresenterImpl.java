package me.nidheesh.diagonalmachinetest.mvp;

import android.content.Context;

import me.nidheesh.diagonalmachinetest.model.Content;
import me.nidheesh.diagonalmachinetest.model.Page;

/**
 * Created by nidheesh on 27/09/2016.
 **/
public class MoviePresenterImpl implements MoviePresenter, MoviePresenter.GetList {

    private MovieView view;
    private MovieInteractor interactor;

    public MoviePresenterImpl(MovieView view) {
        this.view = view;
        this.interactor = new MovieInteractorImpl();
    }

    @Override
    public void getMovies(Context context, int pageNum) {
        interactor.getMovies(context, pageNum, this);
    }

    @Override
    public void onClickList(Content content, Context context) {

    }

    @Override
    public void onLoadList(Page contents) {
        view.updateList(contents);
    }

    @Override
    public void noMoreItems() {
        view.noItemsToShow();
    }
}