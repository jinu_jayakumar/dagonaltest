package me.nidheesh.diagonalmachinetest.mvp;

import android.content.Context;

import me.nidheesh.diagonalmachinetest.model.Content;
import me.nidheesh.diagonalmachinetest.model.Page;

/**
 * Created by nidheesh on 27/09/2016.
 **/
public interface MoviePresenter {

    void getMovies(Context context, int pageNum);

    void onClickList(Content content, Context context);

    interface GetList {

        void onLoadList(Page contents);

        void noMoreItems();
    }
}