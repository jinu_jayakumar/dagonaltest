package me.nidheesh.diagonalmachinetest.mvp;

import me.nidheesh.diagonalmachinetest.model.Page;

/**
 * Created by nidheesh on 27/09/2016.
 **/
public interface MovieView {

    void updateList(Page contents);

    void noItemsToShow();
}
