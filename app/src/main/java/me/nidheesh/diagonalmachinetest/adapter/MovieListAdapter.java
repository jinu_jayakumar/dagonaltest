package me.nidheesh.diagonalmachinetest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import me.nidheesh.diagonalmachinetest.R;
import me.nidheesh.diagonalmachinetest.interfaces.MovieInteractionListener;
import me.nidheesh.diagonalmachinetest.model.Content;
import me.nidheesh.diagonalmachinetest.provider.ResourceProvider;

/**
 * Created by nidheesh on 27/09/2016.
 **/

public class MovieListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MovieListAdapter";
    private int view_type_loading = 2;
    private int view_type_list = 0;


    private ArrayList<Content> contents;
    private boolean showLoadMore = true;
    private MovieInteractionListener mListner;

    public void setShowLoadMore(boolean showLoadMore) {
        this.showLoadMore = showLoadMore;
    }

    public MovieListAdapter(ArrayList<Content> contents, MovieInteractionListener mListner) {
        this.contents = contents;
        this.mListner = mListner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       if (viewType == view_type_loading) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_load_more, parent, false);
            return new MovieListLoadingVH(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_movie_list, parent, false);
            return new MovieListVH(view, mListner);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieListLoadingVH) {
            try {
                mListner.onLoadMore();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (holder instanceof MovieListVH) {
            MovieListVH movieListVH = (MovieListVH) holder;
            movieListVH.mMovieTitle.setText(contents.get(position).name);
            movieListVH.mMovieImageView.setImageResource(ResourceProvider.getResource(contents.get(position).posterImage));
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == contents.size()) {
            return view_type_loading;
        }
        return view_type_list;
    }

    @Override
    public int getItemCount() {
        if (contents == null) {
            return 0;
        }
        if (!showLoadMore) {
            return contents.size();
        }
        return contents.size() + 1;
    }


    public boolean isLoader(int position) {

        return getItemViewType(position) == view_type_loading;
    }

    private static class MovieListVH extends RecyclerView.ViewHolder implements View.OnClickListener {


        private TextView mMovieTitle;
        private final MovieInteractionListener mListener;
        private ImageView mMovieImageView;

        MovieListVH(View itemView, MovieInteractionListener mListener) {
            super(itemView);
            mMovieImageView = (ImageView) itemView.findViewById(R.id.movie_image);
            mMovieTitle = (TextView) itemView.findViewById(R.id.movie_text_view);
            this.mListener = mListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClicked(getAdapterPosition());
        }
    }

    private static class MovieListLoadingVH extends RecyclerView.ViewHolder {


        MovieListLoadingVH(View itemView) {
            super(itemView);

        }
    }


}
