
package me.nidheesh.diagonalmachinetest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ContentItems implements Parcelable {

    @SerializedName("content")
    @Expose
    public ArrayList<Content> content = new ArrayList<>();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.content);
    }

    public ContentItems() {
    }

    protected ContentItems(Parcel in) {
        this.content = in.createTypedArrayList(Content.CREATOR);
    }

    public static final Parcelable.Creator<ContentItems> CREATOR = new Parcelable.Creator<ContentItems>() {
        @Override
        public ContentItems createFromParcel(Parcel source) {
            return new ContentItems(source);
        }

        @Override
        public ContentItems[] newArray(int size) {
            return new ContentItems[size];
        }
    };
}
