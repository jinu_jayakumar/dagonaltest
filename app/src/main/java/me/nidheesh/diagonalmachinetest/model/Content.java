
package me.nidheesh.diagonalmachinetest.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Content implements Parcelable {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("poster-image")
    @Expose
    public String posterImage;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.posterImage);
    }

    public Content() {
    }

    protected Content(Parcel in) {
        this.name = in.readString();
        this.posterImage = in.readString();
    }

    public static final Parcelable.Creator<Content> CREATOR = new Parcelable.Creator<Content>() {
        @Override
        public Content createFromParcel(Parcel source) {
            return new Content(source);
        }

        @Override
        public Content[] newArray(int size) {
            return new Content[size];
        }
    };
}
