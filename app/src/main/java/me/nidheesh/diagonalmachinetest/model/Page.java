
package me.nidheesh.diagonalmachinetest.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Page implements Parcelable {

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("total-content-items")
    @Expose
    public String totalContentItems;
    @SerializedName("page-num")
    @Expose
    public String pageNum;
    @SerializedName("page-size")
    @Expose
    public String pageSize;
    @SerializedName("content-items")
    @Expose
    public ContentItems contentItems;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.totalContentItems);
        dest.writeString(this.pageNum);
        dest.writeString(this.pageSize);
        dest.writeParcelable(this.contentItems, flags);
    }

    public Page() {
    }

    protected Page(Parcel in) {
        this.title = in.readString();
        this.totalContentItems = in.readString();
        this.pageNum = in.readString();
        this.pageSize = in.readString();
        this.contentItems = in.readParcelable(ContentItems.class.getClassLoader());
    }

    public static final Parcelable.Creator<Page> CREATOR = new Parcelable.Creator<Page>() {
        @Override
        public Page createFromParcel(Parcel source) {
            return new Page(source);
        }

        @Override
        public Page[] newArray(int size) {
            return new Page[size];
        }
    };
}
