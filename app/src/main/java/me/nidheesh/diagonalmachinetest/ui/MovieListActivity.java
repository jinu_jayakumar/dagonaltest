package me.nidheesh.diagonalmachinetest.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import me.nidheesh.diagonalmachinetest.R;
import me.nidheesh.diagonalmachinetest.adapter.MovieListAdapter;
import me.nidheesh.diagonalmachinetest.interfaces.MovieInteractionListener;
import me.nidheesh.diagonalmachinetest.model.Content;
import me.nidheesh.diagonalmachinetest.model.Page;
import me.nidheesh.diagonalmachinetest.mvp.MoviePresenter;
import me.nidheesh.diagonalmachinetest.mvp.MoviePresenterImpl;
import me.nidheesh.diagonalmachinetest.mvp.MovieView;

public class MovieListActivity extends AppCompatActivity implements MovieView, View.OnClickListener, MovieInteractionListener {

    private RecyclerView mMovieList;
    private View mBackButton;
    private View mSearchButton;
    private TextView mHeading;
    private View mProgress;
    private MoviePresenter mMoviePresenter;
    private int pageNum = 0;
    private ArrayList<Content> contents;
    private MovieListAdapter mMovieListAdapter;
    private GridLayoutManager mGridLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initModel();
        initView();
        initController();
    }

    private void initModel() {
        contents = new ArrayList<>();
    }

    private void initController() {
        mGridLayout = new GridLayoutManager(this, 3);
        mMovieList.setLayoutManager(mGridLayout);
        mMovieListAdapter = new MovieListAdapter(contents, this);
        mGridLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mMovieListAdapter.isLoader(position) ? mGridLayout.getSpanCount() : 1;
            }
        });
        mMovieList.setAdapter(mMovieListAdapter);
        mMoviePresenter = new MoviePresenterImpl(this);
        mBackButton.setOnClickListener(this);
        mSearchButton.setOnClickListener(this);
        mHeading.setText(R.string.app_name);
        mMoviePresenter.getMovies(this, pageNum);
    }

    private void initView() {
        mMovieList = (RecyclerView) findViewById(R.id.movie_list);
        mBackButton = findViewById(R.id.action_back);
        mSearchButton = findViewById(R.id.action_search);
        mHeading = (TextView) findViewById(R.id.title);
        mProgress = findViewById(R.id.progressBar);
    }

    @Override
    public void updateList(Page contents) {
        mProgress.setVisibility(View.GONE);
        this.contents.addAll(contents.contentItems.content);
        pageNum++;
        notifyAdapter();
    }

    private void notifyAdapter() {
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                mMovieListAdapter.notifyDataSetChanged();
            }
        };

        handler.post(r);
    }

    @Override
    public void noItemsToShow() {
        mProgress.setVisibility(View.GONE);
        mMovieListAdapter.setShowLoadMore(false);
        notifyAdapter();
    }

    @Override
    public void onClick(View v) {
        if (v == mBackButton) {
            finish();
        } else if (v == mSearchButton) {
            // TODO: 27/09/2016 search code
        }
    }

    @Override
    public void onLoadMore() {
        mMovieList.postDelayed(new Runnable() {
            @Override
            public void run() {
                mMoviePresenter.getMovies(MovieListActivity.this, pageNum);
            }
        },300);
    }

    @Override
    public void onItemClicked(int position) {
        mMoviePresenter.onClickList(contents.get(position), this);
    }
}
