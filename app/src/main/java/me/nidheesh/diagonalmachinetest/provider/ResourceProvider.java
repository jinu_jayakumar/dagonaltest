package me.nidheesh.diagonalmachinetest.provider;

import me.nidheesh.diagonalmachinetest.R;

/**
 * Created by nidheesh on 27/09/2016.
 **/
public class ResourceProvider {


    public static int getResource(String posterImage) {
        if (posterImage==null){
            return R.drawable.placeholder_for_missing_posters;
        }else
        if (posterImage.equals("poster1.jpg")){
            return R.drawable.poster1;
        }else if (posterImage.equals("poster2.jpg")){
            return R.drawable.poster2;
        }else if (posterImage.equals("poster3.jpg")){
            return R.drawable.poster3;
        }else if (posterImage.equals("poster4.jpg")){
            return R.drawable.poster4;
        }else if (posterImage.equals("poster5.jpg")){
            return R.drawable.poster5;
        }else if (posterImage.equals("poster6.jpg")){
            return R.drawable.poster6;
        }else if (posterImage.equals("poster7.jpg")){
            return R.drawable.poster7;
        }else if (posterImage.equals("poster8.jpg")){
            return R.drawable.poster8;
        }else if (posterImage.equals("poster9.jpg")){
            return R.drawable.poster9;
        }else {
            return R.drawable.placeholder_for_missing_posters;
        }
    }
}
