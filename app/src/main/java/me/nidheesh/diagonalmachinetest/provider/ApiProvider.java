package me.nidheesh.diagonalmachinetest.provider;


import com.google.gson.Gson;

import java.io.InputStream;
import java.util.Scanner;


import me.nidheesh.diagonalmachinetest.model.MovieList;

/**
 * Created by nidheesh on 27/09/2016.
 **/

public class ApiProvider {

    public static MovieList getMovieList(InputStream stream) {
        Scanner s = new Scanner(stream);
        String word = "";
        while (s.hasNext()) {
            word += s.next();
        }
        return new Gson().fromJson(word, MovieList.class);
    }


}
