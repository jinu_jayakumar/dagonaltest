package me.nidheesh.diagonalmachinetest.ui;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import me.nidheesh.diagonalmachinetest.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MovieListActivityTest {

    @Rule
    public ActivityTestRule<MovieListActivity> mActivityTestRule = new ActivityTestRule<>(MovieListActivity.class);

    @Test
    public void movieListActivityTest() {
        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.movie_list),
                        withParent(allOf(withId(R.id.activity_main),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        recyclerView.perform(actionOnItemAtPosition(17, click()));

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.movie_list),
                        withParent(allOf(withId(R.id.activity_main),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        recyclerView2.perform(actionOnItemAtPosition(40, click()));

        ViewInteraction recyclerView3 = onView(
                allOf(withId(R.id.movie_list),
                        withParent(allOf(withId(R.id.activity_main),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        recyclerView3.perform(actionOnItemAtPosition(52, click()));

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.action_search), withContentDescription("Search"),
                        withParent(allOf(withId(R.id.toolbar),
                                withParent(withId(R.id.activity_main)))),
                        isDisplayed()));
        appCompatImageView.perform(click());

    }

}
