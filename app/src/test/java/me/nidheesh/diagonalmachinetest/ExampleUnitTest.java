package me.nidheesh.diagonalmachinetest;

import com.google.gson.Gson;

import org.junit.Test;

import me.nidheesh.diagonalmachinetest.model.MovieList;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private static String list1="{\n" +            "  \"page\": {\n" +            "    \"title\": \"Romantic Comedy\",\n" +            "    \"total-content-items\" : \"54\",\n" +            "    \"page-num\" : \"1\",\n" +            "    \"page-size\" : \"20\",\n" +            "    \"content-items\": {\n" +            "      \"content\": [\n" +            "        {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Rear Window\",\n" +            "          \"poster-image\": \"poster2.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Family Pot\",\n" +            "          \"poster-image\": \"poster3.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Family Pot\",\n" +            "          \"poster-image\": \"poster2.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Rear Window\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster3.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Rear Window\",\n" +            "          \"poster-image\": \"poster3.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster2.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Family Pot\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        },\n" +            "                {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Rear Window\",\n" +            "          \"poster-image\": \"poster2.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Family Pot\",\n" +            "          \"poster-image\": \"poster3.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Family Pot\",\n" +            "          \"poster-image\": \"poster2.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Rear Window\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster3.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Rear Window\",\n" +            "          \"poster-image\": \"poster3.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster2.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"Family Pot\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        },\n" +            "        {\n" +            "          \"name\": \"The Birds\",\n" +            "          \"poster-image\": \"poster1.jpg\"\n" +            "        }\n" +            "      ]\n" +            "    }\n" +            "  }\n" +            "}";
    private static String list2="{\n" +
            "  \"page\": {\n" +
            "    \"title\": \"Romantic Comedy\",\n" +
            "    \"total-content-items\" : \"54\",\n" +
            "    \"page-num\" : \"2\",\n" +
            "    \"page-size\" : \"20\",\n" +
            "    \"content-items\": {\n" +
            "      \"content\": [\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster6.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster4.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster6.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster6.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster4.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster4.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        },\n" +
            "                {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster6.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster4.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster6.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster6.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster4.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster4.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster5.jpg\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "}";
    private static String list3="{\n" +
            "  \"page\": {\n" +
            "    \"title\": \"Romantic Comedy\",\n" +
            "    \"total-content-items\" : \"27\",\n" +
            "    \"page-num\" : \"3\",\n" +
            "    \"page-size\" : \"14\",\n" +
            "    \"content-items\": {\n" +
            "      \"content\": [\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster9.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster8.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster7.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster9.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster9.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster8.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster7.jpg\"\n" +
            "        },\n" +
            "               {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster9.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"poster8.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster7.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds with an Extra Long Title\",\n" +
            "          \"poster-image\": \"poster9.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Rear Window\",\n" +
            "          \"poster-image\": \"poster9.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"The Birds\",\n" +
            "          \"poster-image\": \"poster8.jpg\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Family Pot\",\n" +
            "          \"poster-image\": \"posterthatismissing.jpg\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "}";
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(1, 22%3);
    }@Test
    public void checkApi() throws Exception {
        MovieList movieList=new Gson().fromJson(list1,MovieList.class);
        assertNotNull(movieList);
        movieList=new Gson().fromJson(list2,MovieList.class);
        assertNotNull(movieList);
        movieList=new Gson().fromJson(list3,MovieList.class);
        assertNotNull(movieList);
    }
}